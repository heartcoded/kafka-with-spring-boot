Cool features

1.Disk not heap
2.Log compaction - Log compaction retains at least the last known value for each record key for a single topic partition. 
Compacted logs are useful for restoring state after a crash or system failure.
3.Balanced partitions & leaders (automatically rebalancing; zookeeper under the covers) 

######

Consumer Group

A consumer group defines the set of consumers that, taken all into account will read the whole topic.
-each consumer alone can read from ANY number of partitions. If you have 1 consumer group with ONLY ONE consumer 
and the topic si partitioned in N partitions the single consumer will read from ALL the N partitions.
-each partition of a topic can be read ONLY BY ONE consumer within a consumer group
-2 consumers that are part of different consumer groups CAN read from THE SAME topic and THE SAME partition, but each
of them will have a DIFFERENT offset.Both will read the same data, but you will process each message twice.
-a consumer group will read ALL THE DATA from one topic ONLY ONCE; each consumer inside the consumer group will read a part of the topic
(a subset of the partitions). If you group all the events that are processed by all the consumers inside a consumer group
you get all messages that were written to that topic.


######

Listeners can be configured to consume specific types of messages by adding a custom filter. This can be done by setting a RecordFilterStrategy to the KafkaListenerContainerFactory:

######
To create messages, first you need to configure a ProducerFactory which sets the strategy for creating Kafka Producer instances.
Then we need a KafkaTemplate which wraps a Producer instance and provides convenience methods for sending messages to kafka Topic

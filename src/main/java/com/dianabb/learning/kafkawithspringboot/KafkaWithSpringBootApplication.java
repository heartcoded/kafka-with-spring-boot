package com.dianabb.learning.kafkawithspringboot;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.dianabb.learning.kafkawithspringboot.model.Greeting;

@SpringBootApplication
public class KafkaWithSpringBootApplication {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(KafkaWithSpringBootApplication.class);

	public static void main(String[] args) throws InterruptedException {
		ConfigurableApplicationContext context = SpringApplication.run(KafkaWithSpringBootApplication.class, args);

		LOGGER.info("Application started...");
		MessageProducer producer = context.getBean(MessageProducer.class);
		MessageListener listener = context.getBean(MessageListener.class);
		/*
		 * Sending a Hello message to topic 'dianatopic'.
		 */
		producer.sendMessage("Hello to Diana Topic!");

		/*
		 * Sending message to a topic with 6 partition, each message to a different
		 * partition. But as per listener configuration, only the messages from
		 * partition 0 and 3 will be consumed.
		 */
		for (int i = 0; i < 6; i++) {
			producer.sendMessageToPartion("Hello To Partioned Topic!", i);
		}
		listener.partitionLatch.await(10, TimeUnit.SECONDS);

		/*
		 * Sending message to 'filtered' topic. As per listener configuration, all
		 * messages with char sequence 'World' will be discarded.
		 */
		producer.sendMessageToFiltered("Hello Diana!");
		producer.sendMessageToFiltered("Hello World!");
		listener.filterLatch.await(10, TimeUnit.SECONDS);

		/*
		 * Sending message to 'greeting' topic. This will send and recieved a java
		 * object with the help of greetingKafkaListenerContainerFactory.
		 */
		producer.sendGreetingMessage(new Greeting("Greetings", "World!"));
		listener.greetingLatch.await(10, TimeUnit.SECONDS);

		context.close();
	}

	@Bean
	public MessageProducer messageProducer() {
		return new MessageProducer();
	}

	@Bean
	public MessageListener messageListener() {
		return new MessageListener();
	}

	public static class MessageProducer {

		@Autowired
		private KafkaTemplate<String, String> kafkaTemplate;

		@Autowired
		private KafkaTemplate<String, Greeting> greetingKafkaTemplate;

		@Value(value = "${message.topic.name}")
		private String topicName;

		@Value(value = "${partitioned.topic.name}")
		private String partionedTopicName;

		@Value(value = "${filtered.topic.name}")
		private String filteredTopicName;

		@Value(value = "${greeting.topic.name}")
		private String greetingTopicName;

		public void sendMessage(String message) {

			ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(topicName, message);
			/// handle the results asynchronously so that the subsequent messages do not
			/// wait for the result of the previous message
			future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

				@Override
				public void onSuccess(SendResult<String, String> result) {
					System.out.println(
							"Sent message=[" + message + "] with offset=[" + result.getRecordMetadata().offset() + "]");
					LOGGER.info("Partition: " + result.getRecordMetadata().partition() + ";Topic: "
							+ result.getRecordMetadata().topic());
				}

				@Override
				public void onFailure(Throwable ex) {
					LOGGER.info("Unable to send message=[" + message + "] to topic "+ topicName +" due to : " + ex.getMessage());
				}
			});
		}

		public void sendMessageToPartion(String message, int partition) {
			kafkaTemplate.send(partionedTopicName, partition, null, message);
		}

		public void sendMessageToFiltered(String message) {
			kafkaTemplate.send(filteredTopicName, message);
		}

		public void sendGreetingMessage(Greeting greeting) {
			greetingKafkaTemplate.send(greetingTopicName, greeting);
		}
	}

	public static class MessageListener {
		/*
		 * using CountDownLatch we can cause a thread to block until other threads have
		 * completed a given task
		 */

		private CountDownLatch latch = new CountDownLatch(3);

		private CountDownLatch partitionLatch = new CountDownLatch(2);

		private CountDownLatch filterLatch = new CountDownLatch(2);

		private CountDownLatch greetingLatch = new CountDownLatch(1);

		@KafkaListener(topics = "${message.topic.name}", groupId = "foo", containerFactory = "fooKafkaListenerContainerFactory")
		public void listenGroupFoo(String message) {
			LOGGER.info("Received Messasge in group 'foo': " + message);
			latch.countDown();
		}

		@KafkaListener(topics = "${message.topic.name}", groupId = "bar", containerFactory = "barKafkaListenerContainerFactory")
		public void listenGroupBar(String message) {
			LOGGER.info("Received Messasge in group 'bar': " + message);
			latch.countDown();
		}

		@KafkaListener(topics = "${message.topic.name}", containerFactory = "headersKafkaListenerContainerFactory")
		public void listenWithHeaders(@Payload String message,
				@Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition, @Header(KafkaHeaders.CONSUMER) KafkaConsumer<String, String> consumer, @Header(KafkaHeaders.OFFSET) Long offset) {
			LOGGER.info("Received Messasge: " + message + " from partition: " + partition+"; consumer: "+consumer+"; OFFSET = "+offset);
			latch.countDown();
		}

		//Only messages from partition 0 and 3 will be consumed (even though we configured 6)
		@KafkaListener(topicPartitions = @TopicPartition(topic = "${partitioned.topic.name}", partitions = { "0",
				"3" }))
		public void listenToParition(@Payload String message,
				@Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
			LOGGER.info("Received Message: " + message + " from partition: " + partition);
			this.partitionLatch.countDown();
		}

		@KafkaListener(topics = "${filtered.topic.name}", containerFactory = "filterKafkaListenerContainerFactory")
		public void listenWithFilter(String message) {
			LOGGER.info("Recieved Message in filtered listener: " + message);
			this.filterLatch.countDown();
		}

		@KafkaListener(topics = "${greeting.topic.name}", containerFactory = "greetingKafkaListenerContainerFactory")
		public void greetingListener(Greeting greeting) {
			LOGGER.info("Recieved greeting message: " + greeting);
			this.greetingLatch.countDown();
		}
	}

}

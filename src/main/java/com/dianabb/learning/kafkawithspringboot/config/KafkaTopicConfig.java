package com.dianabb.learning.kafkawithspringboot.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

@Configuration
public class KafkaTopicConfig {

	
	@Value(value = "${kafka.bootstrapAddress}")
	private String bootstrapAddress;
	@Value(value = "${message.topic.name}")
	private String topicName;
	@Value(value = "${partitioned.topic.name}")
	private String partionedTopicName;
	@Value(value = "${filtered.topic.name}")
	private String filteredTopicName;
	@Value(value = "${greeting.topic.name}")
	private String greetingTopicName;
	
	/**
	 * 
	 * @return
	 */
	@Bean
	public KafkaAdmin kafkaAdmin() {
		Map<String, Object> mapConfigs = new HashMap<>();
		mapConfigs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
		return new KafkaAdmin(mapConfigs);
	}
	
	@Bean
	public NewTopic firstTopic() {
		/**
		 * param1 - topic name
		 * param2 - no of partitions
		 * param3 - replicationFactor 
		 * replicationFactor=1 => one broker(node)
		 */
		return new NewTopic(topicName,1,(short)1);
	}
	@Bean
	public NewTopic partitionedTopic() {	
		return new NewTopic(partionedTopicName,6,(short)1);
	}
	@Bean
	public NewTopic filteredTopic() {
		return new NewTopic(filteredTopicName,1,(short)1);
	}
	@Bean
	public NewTopic greetingTopic() {
		return new NewTopic(greetingTopicName,1,(short)1);
	}
	
}

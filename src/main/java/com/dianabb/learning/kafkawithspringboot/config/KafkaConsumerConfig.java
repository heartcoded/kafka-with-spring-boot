package com.dianabb.learning.kafkawithspringboot.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import com.dianabb.learning.kafkawithspringboot.model.Greeting;

//@EnableKafka needed to enable detection of @KafkaListener on spring managed beans
@EnableKafka
@Configuration
public class KafkaConsumerConfig {

	@Value(value = "${kafka.bootstrapAddress}")
	private String bootstrapAddress;


	public ConsumerFactory<String, String> consumerFactory(String groupId) {
		Map<String, Object> mapConfigs = new HashMap<>();
		mapConfigs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
		mapConfigs.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
		mapConfigs.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		mapConfigs.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		return new DefaultKafkaConsumerFactory<>(mapConfigs);
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, String> fooKafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
		// param1 = groupId
		factory.setConsumerFactory(consumerFactory("foo"));
		return factory;
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, String> barKafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
		// param1 = groupId
		factory.setConsumerFactory(consumerFactory("bar"));
		return factory;
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, String> headersKafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
		// param1 = groupId
		factory.setConsumerFactory(consumerFactory("headers"));
		return factory;
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, String> partitionsKafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
		// param1 = groupId
		factory.setConsumerFactory(consumerFactory("partitions"));
		return factory;
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, String> filterKafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
		// param1 = groupId
		factory.setConsumerFactory(consumerFactory("filter"));
		factory.setRecordFilterStrategy(record -> record.value().contains("World"));
		return factory;
	}

	public ConsumerFactory<String, Greeting> greetingConsumerFactory() {
		Map<String, Object> mapConfigs = new HashMap<>();
		mapConfigs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
		mapConfigs.put(ConsumerConfig.GROUP_ID_CONFIG, "greeting");
		return new DefaultKafkaConsumerFactory<>(mapConfigs, new StringDeserializer(),
				new JsonDeserializer<>(Greeting.class));
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, Greeting> greetingKafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, Greeting> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(greetingConsumerFactory());
		return factory;
	}
}
